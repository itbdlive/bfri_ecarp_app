package com.example.mdsuhelrana.helloandroid.ModelClasses;

import android.content.Context;

import com.example.mdsuhelrana.helloandroid.R;

import java.util.ArrayList;

public final class SpinnerGenerator {


    public static ArrayList<String> getGender(Context context) {
        String[] gender_array = context.getResources().getStringArray(R.array.genders);
        ArrayList<String> genders = new ArrayList<>();
        for (int i = 0; i < gender_array.length; i++) {
            genders.add(gender_array[i]);
        }
        return genders;
    }

    public static ArrayList<String> getMonth(Context context) {
        String[] month_array = context.getResources().getStringArray(R.array.months);
        ArrayList<String> months = new ArrayList<>();
        for (int i = 0; i < month_array.length; i++) {
            months.add(month_array[i]);
        }
        return months;
    }

    public static ArrayList<String> getHormone(Context context) {
        String[] hormon_array = context.getResources().getStringArray(R.array.hormones);
        ArrayList<String> hormones = new ArrayList<>();
        for (int i = 0; i < hormon_array.length; i++) {
            hormones.add(hormon_array[i]);
        }
        return hormones;
    }


//    public static ArrayList<String> getGender(Context context) {
//        String[] gender_array = context.getResources().getStringArray(R.array.genders);
//        ArrayList<String> genders = new ArrayList<>();
//        for (int i = 0; i < gender_array.length; i++) {
//            genders.add(gender_array[i]);
//        }
//        return genders;
//    }



}
