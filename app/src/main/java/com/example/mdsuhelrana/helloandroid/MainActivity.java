package com.example.mdsuhelrana.helloandroid;


import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.mdsuhelrana.helloandroid.AdapterClasses.FishAdapter;
import com.example.mdsuhelrana.helloandroid.AdapterClasses.FormulaAdapter;
import com.example.mdsuhelrana.helloandroid.ModelClasses.Fish;
import com.example.mdsuhelrana.helloandroid.ModelClasses.ListCreation;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

public class MainActivity extends AppCompatActivity {
    private ImageView iv_logo;
    private RecyclerView rv;
    private FormulaAdapter adapter;
    private ArrayList<Fish> list;

    private Toolbar toolbar;
    private DrawerLayout drawer;
    private RecyclerView drawer_rv;
    private FishAdapter fishAdapter;
    private ArrayList<Fish> fishes = new ArrayList<Fish>();

    TextView tv_title;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_formula);

        init();
        createToolbar();
        fishes = ListCreation.createFishArrayList(this);

        list = ListCreation.createFormulalist(this);
        adapter = new FormulaAdapter(this, list);
        rv.setLayoutManager(new GridLayoutManager(this, 2));
        rv.setAdapter(adapter);

        //add drawer layout
        fishAdapter = new FishAdapter(MainActivity.this, fishes);
        LinearLayoutManager llm = new LinearLayoutManager(this);
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        drawer_rv.setLayoutManager(llm);
        drawer_rv.setAdapter(fishAdapter);
    }

    private void createToolbar() {
        toolbar.setBackgroundColor(Color.parseColor("#09dba2"));
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(null);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        View decor = getWindow().getDecorView();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            getWindow().setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark2, this.getTheme()));

        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark2));
        }

    }

    private void init() {
        rv = findViewById(R.id.formula_rv_id);
        tv_title = findViewById(R.id.tv_title);

        drawer_rv = findViewById(R.id.drawer_rv_id);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        drawer = findViewById(R.id.drawer_layout);
        Button back = findViewById(R.id.tool_btn_left);
        Button next = findViewById(R.id.tool_btn_right);
        back.setVisibility(View.INVISIBLE);
        next.setVisibility(View.INVISIBLE);

        iv_logo = findViewById(R.id.iv_logo);
        iv_logo.setVisibility(View.GONE);

        tv_title.setText("মাছের লিস্ট");
        tv_title.setVisibility(View.VISIBLE);

    }

    public void activity_change(View view) {
        switch (view.getId()) {
            case R.id.toolbtn_home:
                startActivity(new Intent(this, MainActivity.class));
                break;
            /*case R.id.tool_btn_left:
                startActivity(new Intent(FormulaActivity.this,MainActivity.class));
                break;
            case R.id.tool_btn_right:
                startActivity(new Intent(FormulaActivity.this, MrigelActivity.class));
                break;*/
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }


}
