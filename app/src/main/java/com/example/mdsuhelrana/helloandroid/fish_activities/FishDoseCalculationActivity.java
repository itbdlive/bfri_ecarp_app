package com.example.mdsuhelrana.helloandroid.fish_activities;

import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.mdsuhelrana.helloandroid.AdapterClasses.FishAdapter;
import com.example.mdsuhelrana.helloandroid.AdapterClasses.SpinnerAdapter;
import com.example.mdsuhelrana.helloandroid.FormulaActivity;
import com.example.mdsuhelrana.helloandroid.MainActivity;
import com.example.mdsuhelrana.helloandroid.ModelClasses.Calculation;
import com.example.mdsuhelrana.helloandroid.ModelClasses.Fish;
import com.example.mdsuhelrana.helloandroid.ModelClasses.ListCreation;
import com.example.mdsuhelrana.helloandroid.ModelClasses.SpinnerGenerator;
import com.example.mdsuhelrana.helloandroid.R;
import com.example.mdsuhelrana.helloandroid.ResultActivity;

import java.util.ArrayList;

public class FishDoseCalculationActivity extends AppCompatActivity {

    private TextView tv_select;
    private EditText et_weight;
    private Spinner gender_spinner,month_spinner,hormone_spinner;
    private SpinnerAdapter genderAdapter,monthAdapter,hormoneAdapter;
    private String gender,month,hormone;
    private Toolbar toolbar;
    private DrawerLayout drawer;
    private RecyclerView drawer_rv;
    private FishAdapter fishAdapter;
    private ArrayList<Fish> fishes=new ArrayList<Fish>();

    TextView tv_title;
    String title = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fish_dose_calculation);

        title = getIntent().getStringExtra("fish_name");

        init();
        createToolbar();
        fishes= ListCreation.createFishArrayList(this);
        tv_select.setText(getResources().getStringArray(R.array.fishes_name)[0]);

        genderAdapter=new SpinnerAdapter(this, SpinnerGenerator.getGender(this));
        gender_spinner.setAdapter(genderAdapter);
        //String genstring=gender_spinner.getSelectedItem().toString();

        gender_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                gender=adapterView.getItemAtPosition(i).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                gender=adapterView.getItemAtPosition(0).toString();
            }
        });


        monthAdapter=new SpinnerAdapter(this,SpinnerGenerator.getMonth(this));
        month_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                month=adapterView.getItemAtPosition(i).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                month=adapterView.getItemAtPosition(0).toString();
            }
        });
        month_spinner.setAdapter(monthAdapter);

        hormoneAdapter=new SpinnerAdapter(this,SpinnerGenerator.getHormone(this));
        hormone_spinner.setAdapter(hormoneAdapter);
        hormone_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                hormone=adapterView.getItemAtPosition(i).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                hormone=adapterView.getItemAtPosition(0).toString();
            }
        });

        //add drawer layout
        fishAdapter = new FishAdapter(this,fishes);
        LinearLayoutManager llm = new LinearLayoutManager(this);
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        drawer_rv.setLayoutManager(llm);
        drawer_rv.setAdapter(fishAdapter);
    }

    private void init() {
        tv_select=findViewById(R.id.select_fish_tv);
        et_weight=findViewById(R.id.fish_weight);
        gender_spinner=findViewById(R.id.select_gender);
        month_spinner=findViewById(R.id.select_month);
        hormone_spinner=findViewById(R.id.select_hormone);

        drawer_rv=findViewById(R.id.drawer_rv_id);
        tv_title=findViewById(R.id.tv_title);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        drawer = findViewById(R.id.drawer_layout);

        //hidden button
        Button back=findViewById(R.id.tool_btn_left);
        Button next=findViewById(R.id.tool_btn_right);
        back.setVisibility(View.INVISIBLE);
        next.setVisibility(View.INVISIBLE);

        tv_title.setText(title);
        tv_title.setVisibility(View.VISIBLE);

    }
    private void createToolbar() {

      /*  toolbar.setTitle("Fish App");
        toolbar.setTitleTextColor(Color.WHITE);*/

        toolbar.setBackgroundColor(Color.parseColor("#09dba2"));
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(null);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        View decor = getWindow().getDecorView();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            getWindow().setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark2, this.getTheme()));

        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark2));
        }

    }

    public void result(View view) {
        String value=et_weight.getText().toString();
        value=value.trim();
        if(!value.equals("")) {
            if(Double.parseDouble(value)>0) {
                Double firstDose = Calculation.katlafirstDose(hormone, gender, month, Double.parseDouble(et_weight.getText().toString()));
                Double secondDose = Calculation.katlaSecondDose(hormone, gender, month, Double.parseDouble(et_weight.getText().toString()));

                if (firstDose == -2) {
                    Toast.makeText(FishDoseCalculationActivity.this, month + " তে কাতলা মাছে " + hormone + " প্রযোজ্য না", Toast.LENGTH_SHORT).show();
                } else if (firstDose==-1){
                    Toast.makeText(FishDoseCalculationActivity.this, "কাতলা মাছে "+hormone+" প্রযোজ্য নয়", Toast.LENGTH_SHORT).show();
                }else {
                    startActivity(new Intent(FishDoseCalculationActivity.this, ResultActivity.class)
                            .putExtra("fish", getResources().getStringArray(R.array.fishes_name)[0])
                            .putExtra("hormone", hormone)
                            .putExtra("gender", gender)
                            .putExtra("month", month)
                            .putExtra("weight", et_weight.getText().toString())
                            .putExtra("firstDose", firstDose)
                            .putExtra("secondDose", secondDose)
                            .putExtra("doseInterval", Calculation.grashcarpDoseInterval(hormone))
                            .putExtra("ovolution", 6));
                }
            }
            else {
                et_weight.setError("ওজন ভুল হয়েছে");
            }
        }else {
            et_weight.setError("ওজন পূরণ করতে হবে");
        }
    }

    public void activity_change(View view) {
        switch (view.getId()){
            case R.id.toolbtn_home:
                startActivity(new Intent(FishDoseCalculationActivity.this, MainActivity.class));
                break;
            /*case R.id.tool_btn_left:
                startActivity(new Intent(FishDoseCalculationActivity.this, FormulaActivity.class));
                break;
            case R.id.tool_btn_right:
                startActivity(new Intent(FishDoseCalculationActivity.this,ResultActivity.class));
                break;*/
        }
    }
    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }
    public void fabClick(View view) {
        startActivity(new Intent(this, FormulaActivity.class));
    }


}