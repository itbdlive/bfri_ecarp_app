package com.example.mdsuhelrana.helloandroid.custom_font;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;

public class DroidSansEdittext extends android.support.v7.widget.AppCompatTextView {

    public DroidSansEdittext(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public DroidSansEdittext(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public DroidSansEdittext(Context context) {
        super(context);
        init();
    }

    public void init() {
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/DroidSans.ttf");
        setTypeface(tf, Typeface.NORMAL);

    }
}
