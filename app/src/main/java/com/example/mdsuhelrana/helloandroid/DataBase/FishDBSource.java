package com.example.mdsuhelrana.helloandroid.DataBase;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

public class FishDBSource {
    private FishDatabaseHelper fdh;
    private SQLiteDatabase db;
    private Context context;
    public FishDBSource(Context context){
        this.context=context;
    }
    public void open(){
        db = fdh.getWritableDatabase();
    }
    public void close(){
        db.close();
    }
    }
