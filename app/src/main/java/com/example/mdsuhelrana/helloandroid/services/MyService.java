package com.example.mdsuhelrana.helloandroid.services;

import android.app.IntentService;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ContentResolver;
import android.content.Intent;
import android.content.Context;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.example.mdsuhelrana.helloandroid.R;
import com.example.mdsuhelrana.helloandroid.activities.TimerActivity;

public class MyService extends IntentService {
    NotificationManager alarmNotificationManager;

    public MyService() {
        super("MyService");
    }
    @Override
    protected void onHandleIntent(Intent intent) {
        sendNotification("Wake Up! Wake Up!");
    }
        private void sendNotification(String msg) {
            Log.d("AlarmService", "Preparing to send notification...: " + msg);
            alarmNotificationManager = (NotificationManager) this
                    .getSystemService(Context.NOTIFICATION_SERVICE);

            PendingIntent contentIntent = PendingIntent.getActivity(this, 0,
                    new Intent(this, TimerActivity.class), 0);
            Uri alarmSound = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE
                    + "://" + this.getPackageName() + "/raw/ring_1");
            NotificationCompat.Builder alamNotificationBuilder = new NotificationCompat.Builder(
                    this).setContentTitle("Alarm")
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setStyle(new NotificationCompat.BigTextStyle().bigText(msg))
                    .setContentText(msg);
            alamNotificationBuilder.setSound(alarmSound);

            alamNotificationBuilder.setContentIntent(contentIntent);
            alarmNotificationManager.notify(1, alamNotificationBuilder.build());
            Log.d("AlarmService", "Notification sent.");
    }
}
