package com.example.mdsuhelrana.helloandroid.custom_font;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;

public class SolaimanLipiTextView extends android.support.v7.widget.AppCompatTextView {

    public SolaimanLipiTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public SolaimanLipiTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public SolaimanLipiTextView(Context context) {
        super(context);
        init();
    }

    public void init() {
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "font/SolaimanLipi.ttf");
        setTypeface(tf, Typeface.NORMAL);

    }
}
