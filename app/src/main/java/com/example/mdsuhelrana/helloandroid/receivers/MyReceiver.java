package com.example.mdsuhelrana.helloandroid.receivers;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.example.mdsuhelrana.helloandroid.activities.TimerActivity;
import com.example.mdsuhelrana.helloandroid.services.MyService;

public class MyReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        Intent intent1=new Intent(context, MyService.class);
        context.startService(intent1);
        setResultCode(Activity.RESULT_OK);
    }
}
