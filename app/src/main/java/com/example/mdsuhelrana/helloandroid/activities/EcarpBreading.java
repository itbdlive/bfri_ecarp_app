package com.example.mdsuhelrana.helloandroid.activities;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Build;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.View;
import android.view.animation.ScaleAnimation;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ScrollView;
import android.widget.TextView;

import com.example.mdsuhelrana.helloandroid.AdapterClasses.FishAdapter;
import com.example.mdsuhelrana.helloandroid.FormulaActivity;
import com.example.mdsuhelrana.helloandroid.MainActivity;
import com.example.mdsuhelrana.helloandroid.ModelClasses.Fish;
import com.example.mdsuhelrana.helloandroid.ModelClasses.GestureListener;
import com.example.mdsuhelrana.helloandroid.ModelClasses.ListCreation;
import com.example.mdsuhelrana.helloandroid.R;

import java.util.ArrayList;

public class EcarpBreading extends AppCompatActivity {
    private ArrayList<Integer>imageList=new ArrayList<>();
    private ViewPager viewPager;
    private WebView ecarp_webview;
    private String fileName="ecarpbreading.html";
    private Toolbar toolbar;
    private DrawerLayout drawer;
    private RecyclerView drawer_rv;
    private FishAdapter fishAdapter;
    private ArrayList<Fish>fishes=new ArrayList<Fish>();

    private int[] images= {R.drawable.pic_1,R.drawable.pic_2};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ecarp_breading);
        //id initialization
        init();
        createToolbar();

        fishes= ListCreation.createFishArrayList(this);

        // Zooming control
        ecarp_webview.getSettings().setJavaScriptEnabled(true);
        ecarp_webview.loadUrl("file:///android_asset/" + fileName);
        ecarp_webview.getSettings().setBuiltInZoomControls(true);
        ecarp_webview.getSettings().setDisplayZoomControls(false);
        ecarp_webview.getSettings().setLoadWithOverviewMode(true);
        ecarp_webview.getSettings().setUseWideViewPort(false);
        ecarp_webview.setHorizontalScrollBarEnabled(false);
        ecarp_webview.setWebViewClient(new WebViewClient());

        //add Font
        addFont();

        //add drawer layout
        fishAdapter = new FishAdapter(this,fishes);
        LinearLayoutManager llm = new LinearLayoutManager(this);
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        drawer_rv.setLayoutManager(llm);
        drawer_rv.setAdapter(fishAdapter);

    }
    private void createToolbar() {
        toolbar.setBackgroundColor(Color.parseColor("#09dba2"));
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(null);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        View decor = getWindow().getDecorView();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            getWindow().setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark2, this.getTheme()));

        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark2));
        }
    }

    private void addFont() {
        Typeface custom_font = Typeface.createFromAsset(getAssets(),  "font/sutonyomj.ttf");
    }

    private void init() {
        ecarp_webview=findViewById(R.id.ecarpbreading_html_id);
        drawer_rv=findViewById(R.id.drawer_rv_id);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        drawer = findViewById(R.id.drawer_layout);
    }

    public void activity_change(View view) {
        switch (view.getId()){
            case R.id.toolbtn_home:
                startActivity(new Intent(EcarpBreading.this, MainActivity.class));
                break;
            case R.id.tool_btn_left:
                startActivity(new Intent(EcarpBreading.this,MainActivity.class));
                break;
            case R.id.tool_btn_right:
                startActivity(new Intent(EcarpBreading.this,Acknowledgment.class));
                break;
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }
    public void fabClick(View view) {
        startActivity(new Intent(this, FormulaActivity.class));
    }
}