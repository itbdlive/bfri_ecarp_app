package com.example.mdsuhelrana.helloandroid.AdapterClasses;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.mdsuhelrana.helloandroid.MainActivity;
import com.example.mdsuhelrana.helloandroid.ModelClasses.Submenu;
import com.example.mdsuhelrana.helloandroid.R;
import com.example.mdsuhelrana.helloandroid.activities.Acknowledgment;
import com.example.mdsuhelrana.helloandroid.activities.Developer;
import com.example.mdsuhelrana.helloandroid.activities.EcarpBreading;
import com.example.mdsuhelrana.helloandroid.activities.Institute;
import com.example.mdsuhelrana.helloandroid.activities.Inventor;

import java.util.ArrayList;

public class SubmenuAdapter extends RecyclerView.Adapter<SubmenuAdapter.SubmenuHolder>{
    private Typeface custom_font;
    private Activity activity;
    private ArrayList<Submenu> list;

    public SubmenuAdapter(Activity activity, ArrayList<Submenu> list) {
        this.list = list;
        this.activity = activity;
    }

    @NonNull
    @Override
    public SubmenuAdapter.SubmenuHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());
        View v = inflater.inflate(R.layout.sub_menu, viewGroup, false);
        return new SubmenuAdapter.SubmenuHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final SubmenuAdapter.SubmenuHolder submenuHolder, int position) {
        submenuHolder.img.setImageResource(list.get(position).getImages());
        submenuHolder.fish_name.setText(list.get(position).getTitle());
        custom_font=Typeface.createFromAsset(activity.getAssets(),  "font/sutonyomj.ttf");
        submenuHolder.fish_name.setTypeface(custom_font);

        submenuHolder.fish_name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(activity,"The Item Clicked is: "+submenuHolder.getAdapterPosition(),Toast.LENGTH_SHORT).show();

                    Intent intent = null;
                    switch (submenuHolder.getAdapterPosition()){
                        case 0:
                            intent =  new Intent(activity, EcarpBreading.class);
                            break;
                        case 1:
                            intent =  new Intent(activity, Institute.class);
                            break;
                        case 2:
                            intent =  new Intent(activity, Acknowledgment.class);
                            break;
                        case 3:
                            intent =  new Intent(activity, Inventor.class);
                            break;
                        case 4:
                            intent =  new Intent(activity, Developer.class);
                            break;
                        default:
                            intent =  new Intent(activity, MainActivity.class);
                            break;
                    }
                    activity.startActivity(intent);

            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    public class SubmenuHolder extends RecyclerView.ViewHolder{
        ImageView img;
        TextView fish_name;

        public SubmenuHolder(@NonNull View itemView) {
            super(itemView);
            img = itemView.findViewById(R.id.submenu_image_id);
            fish_name = itemView.findViewById(R.id.submenu_title_id);
        }
    }
}
