package com.example.mdsuhelrana.helloandroid.services;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.CountDownTimer;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.example.mdsuhelrana.helloandroid.CountdownActivity;
import com.example.mdsuhelrana.helloandroid.DataBase.FishDatabaseHelper;
import com.example.mdsuhelrana.helloandroid.FormulaActivity;
import com.example.mdsuhelrana.helloandroid.R;
import com.example.mdsuhelrana.helloandroid.ResultActivity;
import com.example.mdsuhelrana.helloandroid.activities.TimerActivity;

public class TimerService extends Service {
    private final static String TAG = "BroadcastService";
    public static final String COUNTDOWN_BR = "com.example.mdsuhelrana.helloandroid.services.countdown_br";
    Intent bi = new Intent(COUNTDOWN_BR);
    CountDownTimer cdt=null;
    boolean timerRunning;
    private SharedPreferences prf;
    private SharedPreferences.Editor editor;
    int count=0;

    // handle database
    private FishDatabaseHelper fdh;
    private String btn_state;

    public TimerService() {
    }

    @Override
    public void onCreate() {
        super.onCreate();

        fdh=new FishDatabaseHelper(this);
        btn_state=fdh.getState();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.e("start command", "onstart command");
        super.onStartCommand(intent, flags, startId);
        startTimer(intent.getIntExtra("doseInterval",0),intent.getStringExtra("intervalString"));
        return START_STICKY;
    }

    private void startTimer(int doseInterval, final String intervalString) {
        //Toast.makeText(this, "dose inter val is "+value, Toast.LENGTH_SHORT).show();
        cdt = new CountDownTimer(60000, 1000) {
            @Override
            public void onTick(long untilfinished) {
                bi.putExtra("coundown", untilfinished);
                bi.putExtra("btnvisible",0);
                if(intervalString.equals("interval")){
                    bi.putExtra("intervalString","১ম ও ২য় ডোজের মধ্যকার সময়");
                }else {
                    bi.putExtra("intervalString","ওভোলেশনের সময়");
                }
                sendBroadcast(bi);
                count++;
                if (count == 1) {
                    timerRunning = true;
                    prf = getSharedPreferences("timer", MODE_PRIVATE);
                    editor = prf.edit();
                    if (timerRunning) {
                        editor.putBoolean("timerRunning", timerRunning);
                        editor.apply();
                    }
                }
            }

            @Override
            public void onFinish() {
                bi.putExtra("coundown",0);
                if(!intervalString.equals("ovolution")){
                    bi.putExtra("btnvisible",1);
                    bi.putExtra("intervalString"," ১ম ও ২য় ডোজের মধ্যকার সময় শেষ");
                    sendNotification("১ম ও ২য় ডোজের মধ্যকার সময় শেষ");
                    fdh.updateState("on");

                }else {
                    bi.putExtra("intervalString", "ওভোলেশনের সময় শেষ");
                    sendNotification("ওভোলেশনের সময় শেষ");
                    fdh.updateState("off");
                }
                sendBroadcast(bi);
                count = 0;
                editor.clear();
                editor.apply();
                stopSelf();
            }
        };
        cdt.start();
        Log.e("onstart", "onstart");
    }

    // here is the notification code
    private void sendNotification(String intervalString) {
        NotificationManager notificationManager = (NotificationManager)
                this.getSystemService(Context.NOTIFICATION_SERVICE);
        int notificationId = 1;
        String channelId = "channel-01";
        String channelName = "Channel Name";
        int importance = NotificationManager.IMPORTANCE_HIGH;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            NotificationChannel mChannel = new NotificationChannel(
                    channelId, channelName, importance);
            notificationManager.createNotificationChannel(mChannel);
        }

        Intent intent=new Intent(getApplicationContext(),CountdownActivity.class);
        intent.putExtra("doseInterval",-5);
        intent.setFlags(Intent. FLAG_ACTIVITY_CLEAR_TOP | Intent. FLAG_ACTIVITY_SINGLE_TOP );

        Uri alarmSound = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE
                + "://" + this.getPackageName() + "/raw/ring_1");

        MediaPlayer mp = MediaPlayer. create (getApplicationContext(), alarmSound);
        mp.start();

        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this, channelId)
                .setSmallIcon(R.drawable.app_logo)
                .setContentTitle("ডোজ ক্যালকুলেশন")
                .setStyle(new NotificationCompat.BigTextStyle().bigText(intervalString))
                .setContentText(intervalString);
        mBuilder.setAutoCancel(true);
        PendingIntent contentIntent = PendingIntent.getActivity(getApplicationContext(),0, intent,PendingIntent.FLAG_UPDATE_CURRENT);
        mBuilder.setContentIntent(contentIntent);
        notificationManager.notify(notificationId, mBuilder.build());
    }

    @Override
    public void onDestroy() {
        Log.e("Destroy","Destroy");
        super.onDestroy();
        count=0;
        editor.clear();
        editor.apply();
        stopSelf();
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
      return null;
    }

    @Override
    public void onTaskRemoved(Intent rootIntent) {
        Log.e("ontask removed","On Task Removed");
         bi.putExtra("msg","Dont reomve app from task manager");
        sendBroadcast(bi);
        FishDatabaseHelper fdh=new FishDatabaseHelper(this);
        fdh.updateState("off");
        count=0;
        editor.clear();
        editor.apply();
        stopSelf();
        super.onTaskRemoved(rootIntent);
    }
}
