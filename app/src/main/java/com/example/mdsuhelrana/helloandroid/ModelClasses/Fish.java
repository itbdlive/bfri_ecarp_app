package com.example.mdsuhelrana.helloandroid.ModelClasses;

public class Fish {
    private int images;
    private String title;

    public Fish(int images, String title) {
        this.images = images;
        this.title = title;
    }

    public int getImages() {
        return images;
    }

    public void setImages(int images) {
        this.images = images;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
