package com.example.mdsuhelrana.helloandroid.custom_font;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;

public class DroidSansButton extends android.support.v7.widget.AppCompatTextView {

    public DroidSansButton(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public DroidSansButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public DroidSansButton(Context context) {
        super(context);
        init();
    }

    public void init() {
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/DroidSans.ttf");
        setTypeface(tf, Typeface.NORMAL);

    }
}
