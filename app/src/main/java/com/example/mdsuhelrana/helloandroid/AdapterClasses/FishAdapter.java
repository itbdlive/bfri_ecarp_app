package com.example.mdsuhelrana.helloandroid.AdapterClasses;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.mdsuhelrana.helloandroid.CountdownActivity;
import com.example.mdsuhelrana.helloandroid.FormulaActivity;
import com.example.mdsuhelrana.helloandroid.ModelClasses.Fish;
import com.example.mdsuhelrana.helloandroid.ModelClasses.ListCreation;
import com.example.mdsuhelrana.helloandroid.ModelClasses.Submenu;
import com.example.mdsuhelrana.helloandroid.R;
import com.example.mdsuhelrana.helloandroid.activities.ArtificialBreading;
import com.example.mdsuhelrana.helloandroid.activities.BrudFish;
import com.example.mdsuhelrana.helloandroid.activities.CarpDefinition;
import com.example.mdsuhelrana.helloandroid.activities.Fecundity;
import com.example.mdsuhelrana.helloandroid.activities.FishBreadingInfo;
import com.example.mdsuhelrana.helloandroid.activities.HachariAnusangik;
import com.example.mdsuhelrana.helloandroid.activities.HachariEgg;
import com.example.mdsuhelrana.helloandroid.activities.HachariFish;
import com.example.mdsuhelrana.helloandroid.activities.HachariSthapon;
import com.example.mdsuhelrana.helloandroid.activities.HormonInjection;
import com.example.mdsuhelrana.helloandroid.activities.NaturalStriping;
import com.example.mdsuhelrana.helloandroid.activities.NisiktoEgg;
import com.example.mdsuhelrana.helloandroid.activities.RenuPrapti;

import java.util.ArrayList;

public class FishAdapter extends  RecyclerView.Adapter<FishAdapter.FishViewHolder>{
    private Typeface custom_font;
    private Activity activity;
    private ArrayList<Fish> fishes;
    private ArrayList<Submenu> submenus = new ArrayList<Submenu>();
    private SubmenuAdapter submenuAdapter;

    public FishAdapter(Activity activity, ArrayList<Fish> fishes) {
        this.activity = activity;
        this.fishes = fishes;
    }

    @Override
    public FishViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View v = inflater.inflate(R.layout.fish_single_row2,parent,false);
        return new FishAdapter.FishViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final FishViewHolder holder, final int position) {
        holder.fishImgae.setImageResource(fishes.get(position).getImages());
        holder.fishTitle.setText(fishes.get(position).getTitle());
        custom_font=Typeface.createFromAsset(activity.getAssets(),  "font/sutonyomj.ttf");
        holder.fishTitle.setTypeface(custom_font);

        holder.fishTitle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(activity,"The Item Clicked is: "+holder.getAdapterPosition(),Toast.LENGTH_SHORT).show();
                if(holder.getAdapterPosition() == 0){
                    if(holder.submenu.getVisibility() == View.INVISIBLE){
                        submenus = ListCreation.createSubmenuArrayList(activity);
                        submenuAdapter = new SubmenuAdapter(activity, submenus);
                        LinearLayoutManager llm = new LinearLayoutManager(activity);
                        holder.submenu_rv.setLayoutManager(llm);
                        holder.submenu_rv.setAdapter(submenuAdapter);
                        holder.submenu.setVisibility(View.VISIBLE);
                        //Toast.makeText(activity,"The Item Clicked is: 200"+holder.getAdapterPosition(),Toast.LENGTH_SHORT).show();
                    }else {

                        submenus = new ArrayList<>();
                        submenuAdapter = new SubmenuAdapter(activity, submenus);
                        LinearLayoutManager llm = new LinearLayoutManager(activity);
                        holder.submenu_rv.setLayoutManager(llm);
                        holder.submenu_rv.setAdapter(submenuAdapter);
                        holder.submenu.setVisibility(View.INVISIBLE);
                    }
                }else{
                    Intent intent = null;
                    switch (holder.getAdapterPosition()){
                        case 1:
                            intent =  new Intent(activity, CarpDefinition.class);
                            break;
                        case 2:
                            intent =  new Intent(activity, Fecundity.class);
                            break;
                        case 3:
                            intent =  new Intent(activity, FishBreadingInfo.class);
                            break;
                        case 4:
                            intent =  new Intent(activity, ArtificialBreading.class);
                            break;
                        case 5:
                            intent =  new Intent(activity, HachariSthapon.class);
                            break;
                        case 6:
                            intent =  new Intent(activity, HachariAnusangik.class);
                            break;
                        case 7:
                            intent =  new Intent(activity, BrudFish.class);
                            break;
                        case 8:
                            intent =  new Intent(activity, HachariEgg.class);
                            break;
                        case 9:
                            intent =  new Intent(activity, HormonInjection.class);
                            break;
                        case 10:
                            intent =  new Intent(activity, NaturalStriping.class);
                            break;
                        case 11:
                            intent =  new Intent(activity, NisiktoEgg.class);
                            break;
                        case 12:
                            intent =  new Intent(activity, RenuPrapti.class);
                            break;
                        case 13:
                            intent =  new Intent(activity, HachariFish.class);
                            break;
                        case 14:
                            intent =  new Intent(activity, FormulaActivity.class);
                            break;
                        default:
                            intent =  new Intent(activity, CountdownActivity.class)
                                    .putExtra("doseInterval",-5);
                            break;
                    }
                    activity.startActivity(intent);
                }

            }
        });






    }

    @Override
    public int getItemCount() {
        return fishes.size();
    }

    public class FishViewHolder extends RecyclerView.ViewHolder {
        LinearLayout submenu;
        RecyclerView submenu_rv;
        ImageView fishImgae;
        TextView fishTitle;

        public FishViewHolder(View itemView) {
            super(itemView);
            submenu = itemView.findViewById(R.id.layout_submenu);
            fishImgae = itemView.findViewById(R.id.fish_image_id);
            fishTitle = itemView.findViewById(R.id.fish_title_id);
            submenu_rv = itemView.findViewById(R.id.submenu_rv_id);

//            itemView.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    Intent intent = null;
//                    switch (getAdapterPosition()){
//                        case 0:
//                            intent =  new Intent(activity, EcarpBreading.class);
//                            break;
//                        case 1:
//                            intent =  new Intent(activity, CarpDefinition.class);
//                            break;
//                        case 2:
//                            intent =  new Intent(activity, Fecundity.class);
//                            break;
//                        case 3:
//                            intent =  new Intent(activity, FishBreadingInfo.class);
//                            break;
//                        case 4:
//                            intent =  new Intent(activity, ArtificialBreading.class);
//                            break;
//                        case 5:
//                            intent =  new Intent(activity, HachariSthapon.class);
//                            break;
//                        case 6:
//                            intent =  new Intent(activity, HachariAnusangik.class);
//                            break;
//                        case 7:
//                            intent =  new Intent(activity, BrudFish.class);
//                            break;
//                        case 8:
//                            intent =  new Intent(activity, HachariEgg.class);
//                            break;
//                        case 9:
//                            intent =  new Intent(activity, HormonInjection.class);
//                            break;
//                        case 10:
//                            intent =  new Intent(activity, NaturalStriping.class);
//                            break;
//                        case 11:
//                            intent =  new Intent(activity, NisiktoEgg.class);
//                            break;
//                        case 12:
//                            intent =  new Intent(activity, RenuPrapti.class);
//                            break;
//                        case 13:
//                            intent =  new Intent(activity, HachariFish.class);
//                            break;
//                        case 14:
//                            intent =  new Intent(activity, FormulaActivity.class);
//                            break;
//                        default:
//                            intent =  new Intent(activity, CountdownActivity.class)
//                            .putExtra("doseInterval",-5);
//                            break;
//                    }
//                    activity.startActivity(intent);
//                }
//            });
        }

    }
}