package com.example.mdsuhelrana.helloandroid.activities;

import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.example.mdsuhelrana.helloandroid.DataBase.FishDatabaseHelper;
import com.example.mdsuhelrana.helloandroid.MainActivity;
import com.example.mdsuhelrana.helloandroid.R;
import com.example.mdsuhelrana.helloandroid.receivers.MyReceiver;

import java.util.Calendar;

public class TimerActivity extends AppCompatActivity {
    FishDatabaseHelper fdh;
    private MediaPlayer mp;
    AlarmManager alarmManager;
    private PendingIntent pendingIntent;
    private TimePicker alarmTimePicker;
    private static TimerActivity inst;
    private TextView alarmTextView;
    private Button btn_start,btn_stop;
    private EditText et_time_interval;
    private String btn_state;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_timer);
        fdh=new FishDatabaseHelper(this);

        btn_state=fdh.getState();
        Toast.makeText(this, "value is "+btn_state, Toast.LENGTH_SHORT).show();

        alarmTimePicker =findViewById(R.id.alarmTimePicker);
        alarmTextView = findViewById(R.id.alarmText);
        btn_start=findViewById(R.id.start_id);
        btn_stop=findViewById(R.id.stop_id);


        if (btn_state.equals("off")){
           btn_start.setVisibility(View.VISIBLE);
            btn_stop.setVisibility(View.INVISIBLE);


        }else{
            btn_start.setVisibility(View.INVISIBLE);
            btn_stop.setVisibility(View.VISIBLE);
        }
        et_time_interval=findViewById(R.id.time_interval_id);
        alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
    }
    public void setAlarmText(String alarmText) {
        alarmTextView.setText(alarmText);
    }

    public void start(View view) {
        String value=et_time_interval.getText().toString();
        value=value.trim();
        if(!value.equals("")){
           int time_interval=Integer.parseInt(value);
           if(time_interval>=1 && time_interval<=100){
               Calendar calendar = Calendar.getInstance();
               calendar.set(Calendar.HOUR_OF_DAY, alarmTimePicker.getCurrentHour());
               calendar.set(Calendar.MINUTE, alarmTimePicker.getCurrentMinute());
               Intent myIntent = new Intent(TimerActivity.this, MyReceiver.class);
               pendingIntent = PendingIntent.getBroadcast(TimerActivity.this, 0, myIntent, PendingIntent.FLAG_UPDATE_CURRENT|Intent.FILL_IN_DATA);
               alarmManager.setRepeating(AlarmManager.RTC, calendar.getTimeInMillis(), time_interval * 1000, pendingIntent);
               boolean status = fdh.updateState("on");
               if (status == true) {
                   Toast.makeText(TimerActivity.this, "Updated Successfully", Toast.LENGTH_SHORT).show();
               }
               startActivity(new Intent(TimerActivity.this, MainActivity.class));
           }else {
               et_time_interval.setError("invalid hour");
           }
        }else {
            et_time_interval.setError("input field is required");
        }
    }

    public void stop(View view) {
        Intent intent=new Intent(TimerActivity.this,MyReceiver.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(this.getApplicationContext(), 0, intent, PendingIntent.FLAG_UPDATE_CURRENT| Intent.FILL_IN_DATA);
        if(alarmManager!=null) {
            alarmManager.cancel(pendingIntent);
        }
        setAlarmText("");

        boolean status=fdh.updateState("off");
        if (status==true){
            Toast.makeText(TimerActivity.this, "Stop Button updated successfully", Toast.LENGTH_SHORT).show();
        }
        startActivity(new Intent(TimerActivity.this, MainActivity.class));
    }

    public void setTone(View view) {
        AlertDialog.Builder builder=new AlertDialog.Builder(this);
        builder.setTitle("Set Your choice");
        LayoutInflater inflater=LayoutInflater.from(this);
        View v=inflater.inflate(R.layout.dialog_tone_setting,null,false);
        RadioGroup rg=v.findViewById(R.id.rg_id);
        rg.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                int id=radioGroup.getCheckedRadioButtonId();
                if(id==R.id.ring_1_id){
                    toneStop();
                    mp=MediaPlayer.create(TimerActivity.this,R.raw.ring_1);
                    mp.start();
                }else if(id==R.id.ring_2_id){
                    toneStop();
                    mp=MediaPlayer.create(TimerActivity.this,R.raw.ring_2);
                    mp.start();
                }else if(id==R.id.ring_3_id){
                    toneStop();
                    mp=MediaPlayer.create(TimerActivity.this,R.raw.ring_3);
                    mp.start();
                }else if(id==R.id.ring_4_id){
                    toneStop();
                    mp=MediaPlayer.create(TimerActivity.this,R.raw.ring_4);
                    mp.start();
                }else if(id==R.id.ring_5_id){
                    toneStop();
                    mp=MediaPlayer.create(TimerActivity.this,R.raw.ring_5);
                    mp.start();
                }else if(id==R.id.ring_6_id){
                    toneStop();
                    mp=MediaPlayer.create(TimerActivity.this,R.raw.ring_6);
                    mp.start();
                }
            }
        });
        builder.setView(v);
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                mp.stop();

            }
        });
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

            }
        });
        builder.show();

    }

    private void toneStop() {
        if(mp!=null){
            mp.stop();
            mp.release();
            mp=null;
        }
    }
}
