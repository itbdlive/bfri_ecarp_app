package com.example.mdsuhelrana.helloandroid.AdapterClasses;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.mdsuhelrana.helloandroid.R;

import java.util.ArrayList;

public class SpinnerAdapter extends ArrayAdapter<String> {
    private ArrayList<String>spinnerList;
    private Context context;
    public SpinnerAdapter(@NonNull Context context,ArrayList<String>spinnerList) {
        super(context, 0,spinnerList);
        this.context=context;
        this.spinnerList=spinnerList;
    }
    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        return initView(position, convertView, parent);
    }
    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        return initView(position, convertView, parent);
    }
    private View initView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.spinner_single_row_custom, parent, false);
        }
       final TextView textViewName = convertView.findViewById(R.id.spinner_text_id);
            textViewName.setText(spinnerList.get(position));
        return convertView;
    }
}
