package com.example.mdsuhelrana.helloandroid.DataBase;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.annotation.Nullable;

public class FishDatabaseHelper extends SQLiteOpenHelper {

    public static final String DATABASE_NAME = "fish_db";
    public static final int DATABASE_VERSION = 1;

    public static final String TABLE_BTN_STATE = "btn_state";
    public static final String COL_ID = "_id";
    public static final String COL_STATE = "state";

    public static final String CREATE_TABLE_BTN_STATE = "create table "+TABLE_BTN_STATE+"("+
            COL_ID+" integer primary key, "+
            COL_STATE+" text not null);";

    public FishDatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE_BTN_STATE);

        ContentValues values = new ContentValues();
        values.put(COL_STATE,"off");
        db.insert(TABLE_BTN_STATE,null,values);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {
        db.execSQL("drop table if exists "+TABLE_BTN_STATE);
        onCreate(db);

    }
    public String getState(){
        SQLiteDatabase db=this.getWritableDatabase();
        String sql = "SELECT " + COL_STATE + "  FROM " + TABLE_BTN_STATE  + " WHERE " + COL_ID + " = '" +  1 + "'";
        Cursor cur = db.rawQuery(sql, null);
        String value="";
        if (cur.moveToFirst()) {
            do {
                value =(cur.getString(0));
            } while (cur.moveToNext());
        }else{
            throw new NullPointerException();
        }
        cur.close();
        db.close();
        return value;
    }
    public boolean updateState(String value){
        SQLiteDatabase db=this.getWritableDatabase();
        ContentValues cv=new ContentValues();
        cv.put(COL_STATE,value);
        int updatedrow =db.update(TABLE_BTN_STATE, cv, COL_ID + "="+1,null );
        this.close();
        if(updatedrow>0){
        return true;
        }else {
            return false;
        }
    }
}
