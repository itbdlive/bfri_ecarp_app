package com.example.mdsuhelrana.helloandroid.ModelClasses;

import android.content.Context;

import com.example.mdsuhelrana.helloandroid.R;

import java.util.ArrayList;

public final class ListCreation {
    public static ArrayList<Fish> createFishArrayList(Context context) {
        int[] images = {R.drawable.carp1,
                R.drawable.carp2,
                R.drawable.carp3,
                R.drawable.carp4,
                R.drawable.carp5,
                R.drawable.carp6,
                R.drawable.carp7,
                R.drawable.carp8,
                R.drawable.carp9,
                R.drawable.carp10,
                R.drawable.carp11,
                R.drawable.carp12,
                R.drawable.carp13,
                R.drawable.carp14,
                R.drawable.dose_calculation,
                R.drawable.time_count};
        String[] title = context.getResources().getStringArray(R.array.text_array);
        ArrayList<Fish> list = new ArrayList<>();
        for (int i = 0; i < title.length; i++) {
            list.add(new Fish(images[i], title[i]));
        }
        return list;
    }

    public static ArrayList<Submenu> createSubmenuArrayList(Context context) {
        int[] images = {R.drawable.right_arrow_2,
                R.drawable.right_arrow_2,
                R.drawable.right_arrow_2,
                R.drawable.right_arrow_2,
                R.drawable.right_arrow_2,
                };
        String[] title = context.getResources().getStringArray(R.array.submenu_array);
        ArrayList<Submenu> list = new ArrayList<>();
        for (int i = 0; i < title.length; i++) {
            list.add(new Submenu(images[i], title[i]));
        }
        return list;
    }

    public static ArrayList<Integer> createImgList(int[] images) {

        ArrayList<Integer> list = new ArrayList<>();

        for (int i = 0; i < images.length; i++) {
            list.add(images[i]);
        }
        return list;
    }


    public static ArrayList<Fish> createFormulalist(Context context) {

        int[] images = {R.drawable.katla,
                R.drawable.rui,
                R.drawable.mrigel,
                R.drawable.bighead,
                R.drawable.grashcarp,
                R.drawable.silvercarp,
                R.drawable.rajputi,
                R.drawable.kalibaus,
                R.drawable.carpio_2};


        String[] title = context.getResources().getStringArray(R.array.fishes_name);
        ArrayList<Fish> list = new ArrayList<>();
        for (int i = 0; i < title.length; i++) {
            list.add(new Fish(images[i], title[i]));
        }
        return list;
    }
}
