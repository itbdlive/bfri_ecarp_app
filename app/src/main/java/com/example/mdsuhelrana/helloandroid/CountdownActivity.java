package com.example.mdsuhelrana.helloandroid;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Build;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.mdsuhelrana.helloandroid.AdapterClasses.FishAdapter;
import com.example.mdsuhelrana.helloandroid.DataBase.FishDatabaseHelper;
import com.example.mdsuhelrana.helloandroid.ModelClasses.Fish;
import com.example.mdsuhelrana.helloandroid.ModelClasses.ListCreation;
import com.example.mdsuhelrana.helloandroid.services.TimerService;

import java.util.ArrayList;
import java.util.Locale;

public class CountdownActivity extends AppCompatActivity {
    // set countdown timer
    private TextView mTextViewCountDown,timeMesurement;
    private boolean timerRunning;

    private Toolbar toolbar;
    private DrawerLayout drawer;
    private RecyclerView drawer_rv;
    private FishAdapter fishAdapter;
    private ArrayList<Fish> fishes=new ArrayList<Fish>();
    private ProgressBar mprogressbar;
    private int doseInterval;
    private int doseOvolution;
    private Button rdoseCalculation;

    private String formatedtime;
    private String strInterval;

    //handle database
    private FishDatabaseHelper fdh;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_countdown);
        doseInterval=getIntent().getIntExtra("doseInterval",0);
        doseOvolution=getIntent().getIntExtra("doseOvolution",0);
        init();
        createToolbar();
        fishes= ListCreation.createFishArrayList(this);

       // set coundown timer

        SharedPreferences prf = getSharedPreferences("timer", MODE_PRIVATE);
        timerRunning = prf.getBoolean("timerRunning", false);
        //Toast.makeText(this, "value is " + timerRunning, Toast.LENGTH_SHORT).show();
        if (!timerRunning) {
            if(doseInterval==0) {
                startService(new Intent(CountdownActivity.this, TimerService.class)
                        .putExtra("doseInterval", doseOvolution)
                        .putExtra("intervalString","ovolution"));

            }else if (doseInterval==-5){
               // nothing to do
            }
            else {
                startService(new Intent(CountdownActivity.this, TimerService.class)
                        .putExtra("doseInterval", doseInterval)
                        .putExtra("intervalString","interval"));
            }
        }

        //add drawer layout
        fishAdapter = new FishAdapter(this,fishes);
        LinearLayoutManager llm = new LinearLayoutManager(this);
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        drawer_rv.setLayoutManager(llm);
        drawer_rv.setAdapter(fishAdapter);
    }
    private void init() {
        mTextViewCountDown = findViewById(R.id.text_view_countdown);
        timeMesurement=findViewById(R.id.time_mesurment);
        drawer_rv=findViewById(R.id.drawer_rv_id);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        drawer = findViewById(R.id.drawer_layout);
        //hidden button
        Button back=findViewById(R.id.tool_btn_left);
        Button next=findViewById(R.id.tool_btn_right);
        back.setVisibility(View.INVISIBLE);
        next.setVisibility(View.INVISIBLE);
        rdoseCalculation=findViewById(R.id.rdoseCalculation);
        mprogressbar=findViewById(R.id.progressbar);

        //mprogressbar.setMax(doseInterval*60*60*100);

    }
    private void createToolbar() {
        toolbar.setBackgroundColor(Color.parseColor("#09dba2"));
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(null);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        View decor = getWindow().getDecorView();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            getWindow().setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark2, this.getTheme()));

        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark2));
        }

    }

    public void activity_change(View view) {
        switch (view.getId()){
            case R.id.toolbtn_home:
                startActivity(new Intent(CountdownActivity.this, MainActivity.class));
                break;
            /*case R.id.tool_btn_left:
                startActivity(new Intent(CountdownActivity.this, ResultActivity.class));
                break;
            case R.id.tool_btn_right:
                startActivity(new Intent(CountdownActivity.this,MainActivity.class));
                break;*/
        }
    }

    private BroadcastReceiver br=new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            updateUi(intent);
            strInterval=intent.getStringExtra("intervalString");
            timeMesurement.setText(strInterval);
            int visibility=intent.getIntExtra("btnvisible",0);
            if(visibility==1){
                rdoseCalculation.setVisibility(View.VISIBLE);
            }
        }
    };

    private void updateUi(Intent intent) {
        if (intent.getExtras()!=null) {
            long timeInmillis = intent.getLongExtra("coundown", 0);
            mprogressbar.setProgress((int)timeInmillis);

            int second= (int) (timeInmillis / 1000 % 60);
            int minute= (int) (timeInmillis / (60 * 1000) % 60);
            int hour= (int) (timeInmillis / (60 * 60 * 1000) % 24);
            formatedtime = String.format(Locale.getDefault(), "%02d : %02d : %02d", hour, minute,second);
            mTextViewCountDown.setText(formatedtime);
        }else {
            Toast.makeText(this, "intent is null", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver(br,new IntentFilter(TimerService.COUNTDOWN_BR));
        mTextViewCountDown.setText("00 : 00 : 00");
        mprogressbar.setProgress(0);

        fdh=new FishDatabaseHelper(CountdownActivity.this);
        String btn_state=fdh.getState();
        if(btn_state.equals("on")){
            timeMesurement.setText("১ম ও ২য় ডোজের মধ্যকার সময় শেষ");
            rdoseCalculation.setVisibility(View.VISIBLE);
        }else {
            timeMesurement.setText("ওভোলেশনের সময় শেষ");
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(br);
    }

    @Override
    protected void onStop() {
        super.onStop();
        try {
            unregisterReceiver(br);
        }catch (Exception e){
            Log.e("Exception =========",e.getMessage());
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    public void doseCalculation(View view) {
        startService(new Intent(CountdownActivity.this, TimerService.class)
                .putExtra("doseInterval", doseOvolution)
                .putExtra("intervalString","ovolution"));
        rdoseCalculation.setVisibility(View.GONE);
        fdh.updateState("off");
    }
    public void fabClick(View view) {
        startActivity(new Intent(this,FormulaActivity.class));
    }
}
