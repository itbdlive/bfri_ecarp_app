package com.example.mdsuhelrana.helloandroid;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class SplashScreanActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screan);

        View decor = getWindow().getDecorView();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            getWindow().setStatusBarColor(getResources().getColor(R.color.white, this.getTheme()));

        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(getResources().getColor(R.color.white));
        }


        ImageView img = findViewById(R.id.app_logo_id);
        ImageView imageView = findViewById(R.id.imageView);
        TextView textView2 = findViewById(R.id.textView2);
        Animation alpha_anim = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.alpha_anim);
        img.startAnimation(alpha_anim);
        Thread timer=new Thread(){
            @Override
            public void run() {

                try {
                    sleep(4000);
                    Intent intent=new Intent(SplashScreanActivity.this,MainActivity.class);
                    startActivity(intent);
                    finish();
                    super.run();
                } catch (InterruptedException e) {
                    e.printStackTrace();

                }

            }
        };
        timer.start();


        ///
        img.setAnimation(AnimationUtils.loadAnimation(this, R.anim.fade_scale_animation));
        imageView.setAnimation(AnimationUtils.loadAnimation(this, R.anim.fade_scale_animation));
        textView2.setAnimation(AnimationUtils.loadAnimation(this, R.anim.fade_scale_animation));
        ///


    }


    public static JSONArray loadJSONArray_fish(Context context) {
        StringBuilder stringBuilder = new StringBuilder();
        InputStream inputStream = context.getResources().openRawResource(R.raw.fish_json_final);
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));

        String line;
        try {
            while ((line = bufferedReader.readLine()) != null) {
                stringBuilder.append(line);
            }
            JSONObject jsonObject = new JSONObject(stringBuilder.toString());
            return jsonObject.getJSONArray("fish_list");

        } catch (IOException | JSONException e) {
            e.printStackTrace();
        }

        return null;
    }


}
