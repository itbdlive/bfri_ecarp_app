package com.example.mdsuhelrana.helloandroid.AdapterClasses;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.mdsuhelrana.helloandroid.ModelClasses.Fish;
import com.example.mdsuhelrana.helloandroid.ModelClasses.ListCreation;
import com.example.mdsuhelrana.helloandroid.ModelClasses.Submenu;
import com.example.mdsuhelrana.helloandroid.R;
import com.example.mdsuhelrana.helloandroid.fish_activities.BigheadCarpActivity;
import com.example.mdsuhelrana.helloandroid.fish_activities.FishDoseCalculationActivity;
import com.example.mdsuhelrana.helloandroid.fish_activities.GhoniaActivity;
import com.example.mdsuhelrana.helloandroid.fish_activities.GrashcarpActivity;
import com.example.mdsuhelrana.helloandroid.fish_activities.KalibausActivity;
import com.example.mdsuhelrana.helloandroid.fish_activities.MrigelActivity;
import com.example.mdsuhelrana.helloandroid.fish_activities.KatlaActivity;
import com.example.mdsuhelrana.helloandroid.fish_activities.RajputiActivity;
import com.example.mdsuhelrana.helloandroid.fish_activities.RuiActivity;
import com.example.mdsuhelrana.helloandroid.fish_activities.SilvarcarpActivity;

import java.util.ArrayList;

public class FormulaAdapter extends RecyclerView.Adapter<FormulaAdapter.FormulaHolder> {
    private Typeface custom_font;
    private Activity activity;
    private ArrayList<Fish> list;



    public FormulaAdapter(Activity activity, ArrayList<Fish> list) {
        this.activity = activity;
        this.list = list;
    }

    @Override
    public FormulaHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View v = inflater.inflate(R.layout.formula_single_row, parent, false);
        return new FormulaHolder(v);
    }

    @Override
    public void onBindViewHolder(FormulaHolder holder, int position) {
        holder.img.setImageResource(list.get(position).getImages());
        holder.fish_name.setText(list.get(position).getTitle());
        custom_font = Typeface.createFromAsset(activity.getAssets(), "font/sutonyomj.ttf");
        holder.fish_name.setTypeface(custom_font);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class FormulaHolder extends RecyclerView.ViewHolder {

        ImageView img;
        TextView fish_name;

        public FormulaHolder(View itemView) {
            super(itemView);
            img = itemView.findViewById(R.id.formula_image_id);
            fish_name = itemView.findViewById(R.id.formula_text_view_id);


//            img.setRotation(180);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    final Intent intent;
                    switch (getAdapterPosition()) {
                        case 0:
                            intent = new Intent(activity, KatlaActivity.class);
                            intent.putExtra("fish_name", "কাতলা");
                            break;
                        case 1:
                            intent = new Intent(activity, RuiActivity.class);
                            intent.putExtra("fish_name", "রুই");
                            break;
                        case 2:
                            intent = new Intent(activity, MrigelActivity.class);
                            intent.putExtra("fish_name", "মৃগেল");
                            break;
                        case 3:
                            intent = new Intent(activity, BigheadCarpActivity.class);
                            intent.putExtra("fish_name", "রুই");
                            break;
                        case 4:
                            intent = new Intent(activity, GrashcarpActivity.class);
                            intent.putExtra("fish_name", "রুই");
                            break;
                        case 5:
                            intent = new Intent(activity, SilvarcarpActivity.class);
                            intent.putExtra("fish_name", "রুই");
                            break;
                        case 6:
                            intent = new Intent(activity, RajputiActivity.class);
                            intent.putExtra("fish_name", "রুই");
                            break;
                        case 7:
                            intent = new Intent(activity, KalibausActivity.class);
                            intent.putExtra("fish_name", "রুই");
                            break;
                        case 8:
                            intent = new Intent(activity, GhoniaActivity.class);
                            intent.putExtra("fish_name", "কার্পিও");
                            break;
                        default:
                            intent = new Intent(activity, KalibausActivity.class);
                            intent.putExtra("fish_name", "কালিবাউস");
                            break;
                    }

//                    switch (getAdapterPosition()) {
//                        case 0:
//                            intent = new Intent(context, FishDoseCalculationActivity.class);
//                            intent.putExtra("fish_name", "কাতলা");
//                            break;
//                        case 1:
//                            intent = new Intent(context, FishDoseCalculationActivity.class);
//                            intent.putExtra("fish_name", "রুই");
//                            break;
//                        case 2:
//                            intent = new Intent(context, FishDoseCalculationActivity.class);
//                            intent.putExtra("fish_name", "মৃগেল");
//                            break;
//                        case 3:
//                            intent = new Intent(context, FishDoseCalculationActivity.class);
//                            intent.putExtra("fish_name", "কার্পিও");
//                            break;
//                        case 4:
//                            intent = new Intent(context, FishDoseCalculationActivity.class);
//                            intent.putExtra("fish_name", "গ্রাস কার্প");
//                            break;
//                        case 5:
//                            intent = new Intent(context, FishDoseCalculationActivity.class);
//                            intent.putExtra("fish_name", "সিলভার কার্প");
//                            break;
//                        case 6:
//                            intent = new Intent(context, FishDoseCalculationActivity.class);
//                            intent.putExtra("fish_name", "রাজপুটি");
//                            break;
//                        case 7:
//                            intent = new Intent(context, FishDoseCalculationActivity.class);
//                            intent.putExtra("fish_name", "কালিবাউস");
//                            break;
//                        case 8:
//                            intent = new Intent(context, FishDoseCalculationActivity.class);
//                            intent.putExtra("fish_name", "বিগহেড কার্প");
//                            break;
//                        default:
//                            intent = new Intent(context, FishDoseCalculationActivity.class);
//                            intent.putExtra("fish_name", "কালিবাউস");
//                            break;
//                    }


                    activity.startActivity(intent);
                }
            });
        }
    }
}
