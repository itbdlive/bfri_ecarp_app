package com.example.mdsuhelrana.helloandroid.ModelClasses;

public final class Calculation {
    // for mrigel fish
    public static double mrigelfirstDose(String hormone, String gender, String month, double weight) {
        if (hormone.equals("পিটুইটারি গ্ল্যান্ড")) {
            if (month.equals("মার্চ-মে") && gender.equals("স্ত্রী")) {
                return 2 * weight;
            } else if (month.equals("জুন-জুলাই") && gender.equals("স্ত্রী")) {
                return 1 * weight;
            } else if (month.equals("আগস্ট-সেপ্টেম্বর") && gender.equals("স্ত্রী")) {
                return (float) (1.5 * weight);
            } else if (month.equals("মার্চ-মে") && gender.equals("পুরুষ")) {
                return -10;

            } else if (month.equals("জুন-জুলাই") && gender.equals("পুরুষ")) {
                return -10;
            } else {
                return (float) (1.5 * weight);
            }
        } else if (hormone.equals("ওভাপ্রিম হরমোন") || hormone.equals("ওভোপিন হরমোন") || hormone.equals("ওভাসিস টি এম হরমোন")) {
//            return (float) 0.25;
            return -10;
        } else if (hormone.equals("সুমাছ")) {
            return -1;
        } else {
            return -1;
        }
    }

    public static double mrigelSecondDose(String hormone, String gender, String month, double weight) {
        if (hormone.equals("পিটুইটারি গ্ল্যান্ড")) {
            if (month.equals("মার্চ-মে") && gender.equals("স্ত্রী")) {
                return 5 * weight;

            } else if (month.equals("জুন-জুলাই") && gender.equals("স্ত্রী")) {
                return 5 * weight;
            } else if (month.equals("আগস্ট-সেপ্টেম্বর") && gender.equals("স্ত্রী")) {
                return 5 * weight;
            } else if (month.equals("মার্চ-মে") && gender.equals("পুরুষ")) {
                return 2 * weight;

            } else if (month.equals("জুন-জুলাই") && gender.equals("পুরুষ")) {
                return 2 * weight;
            } else {
                return 5 * weight;
            }
        } else if (hormone.equals("ওভাপ্রিম হরমোন") || hormone.equals("ওভোপিন হরমোন") || hormone.equals("ওভাসিস টি এম হরমোন")) {
            return (float) 0.25;
        } else if (hormone.equals("সুমাছ")) {
            return -1;
        } else {
            return -1;
        }
    }

    //for katla fish
    public static double katlafirstDose(String hormone, String gender, String month, double weight) {
        if (hormone.equals("পিটুইটারি গ্ল্যান্ড")) {
            if (month.equals("মার্চ-মে") && gender.equals("স্ত্রী")) {
                return 2 * weight;
            } else if (month.equals("জুন-জুলাই") && gender.equals("স্ত্রী")) {
                return (float) (1.5 * weight);
            } else if (month.equals("আগস্ট-সেপ্টেম্বর") && gender.equals("স্ত্রী")) {
                return -1;
            } else if (month.equals("মার্চ-মে") && gender.equals("পুরুষ")) {
                return -10;

            } else if (month.equals("জুন-জুলাই") && gender.equals("পুরুষ")) {
                return -10;
            } else {
                return -2;
            }
        } else if (hormone.equals("ওভাপ্রিম হরমোন") || hormone.equals("ওভোপিন হরমোন") || hormone.equals("ওভাসিস টি এম হরমোন")) {
//            return (float) 0.40;
            return -10;
        } else if (hormone.equals("সুমাছ")) {
            return -1;
        } else {
            return -1;
        }
    }

    public static double katlaSecondDose(String hormone, String gender, String month, double weight) {
        if (hormone.equals("পিটুইটারি গ্ল্যান্ড")) {
            if (month.equals("মার্চ-মে") && gender.equals("স্ত্রী")) {
                return 6 * weight;

            } else if (month.equals("জুন-জুলাই") && gender.equals("স্ত্রী")) {
                return 5 * weight;
            } else if (month.equals("আগস্ট-সেপ্টেম্বর") && gender.equals("স্ত্রী")) {
                return -1;
            } else if (month.equals("মার্চ-মে") && gender.equals("পুরুষ")) {
                return 2 * weight;

            } else if (month.equals("জুন-জুলাই") && gender.equals("পুরুষ")) {
                return 2 * weight;
            } else {
                return -1;
            }
        } else if (hormone.equals("ওভাপ্রিম হরমোন") || hormone.equals("ওভোপিন হরমোন") || hormone.equals("ওভাসিস টি এম হরমোন")) {
            return (float) 0.40;
        } else if (hormone.equals("সুমাছ")) {
            return -1;
        } else {
            return -1;
        }
    }

    //for rui fish
    public static double ruifirstDose(String hormone, String gender, String month, double weight) {
        if (hormone.equals("পিটুইটারি গ্ল্যান্ড")) {
            if (month.equals("মার্চ-মে") && gender.equals("স্ত্রী")) {
                return 2 * weight;
            } else if (month.equals("জুন-জুলাই") && gender.equals("স্ত্রী")) {
                return 2 * weight;
            } else if (month.equals("আগস্ট-সেপ্টেম্বর") && gender.equals("স্ত্রী")) {
                return (float) (1.5 * weight);
            } else if (month.equals("মার্চ-মে") && gender.equals("পুরুষ")) {
                return -10;

            } else if (month.equals("জুন-জুলাই") && gender.equals("পুরুষ")) {
                return -10;
            } else {
                return (float) (1.5 * weight);
            }
        } else if (hormone.equals("ওভাপ্রিম হরমোন") || hormone.equals("ওভোপিন হরমোন") || hormone.equals("ওভাসিস টি এম হরমোন")) {
//            return (float) 0.40;
            return -10;
        } else if (hormone.equals("সুমাছ")) {
            return -1;
        } else {
            return -1;
        }
    }

    public static double ruiSecondDose(String hormone, String gender, String month, double weight) {
        if (hormone.equals("পিটুইটারি গ্ল্যান্ড")) {
            if (month.equals("মার্চ-মে") && gender.equals("স্ত্রী")) {
                return 5 * weight;

            } else if (month.equals("জুন-জুলাই") && gender.equals("স্ত্রী")) {
                return 5 * weight;
            } else if (month.equals("আগস্ট-সেপ্টেম্বর") && gender.equals("স্ত্রী")) {
                return 6 * weight;
            } else if (month.equals("মার্চ-মে") && gender.equals("পুরুষ")) {
                return 2 * weight;

            } else if (month.equals("জুন-জুলাই") && gender.equals("পুরুষ")) {
                return 2 * weight;
            } else {
                return 6 * weight;
            }
        } else if (hormone.equals("ওভাপ্রিম হরমোন") || hormone.equals("ওভোপিন হরমোন") || hormone.equals("ওভাসিস টি এম হরমোন")) {
            return (float) 0.40;
        } else if (hormone.equals("সুমাছ")) {
            return -1;
        } else {
            return -1;
        }
    }

    //for ghonia fish
    public static double ghoniafirstDose(String hormone, String gender, String month, double weight) {
        if (hormone.equals("পিটুইটারি গ্ল্যান্ড")) {
            if (month.equals("মার্চ-মে") && gender.equals("স্ত্রী")) {
                return 2 * weight;
            } else if (month.equals("জুন-জুলাই") && gender.equals("স্ত্রী")) {
                return 2 * weight;
            } else if (month.equals("আগস্ট-সেপ্টেম্বর") && gender.equals("স্ত্রী")) {
                return 2 * weight;
            } else if (month.equals("মার্চ-মে") && gender.equals("পুরুষ")) {
                return -10;

            } else if (month.equals("জুন-জুলাই") && gender.equals("পুরুষ")) {
                return -10;
            } else {
                return 2 * weight;
            }
        } else if (hormone.equals("ওভাপ্রিম হরমোন") || hormone.equals("ওভোপিন হরমোন") || hormone.equals("ওভাসিস টি এম হরমোন")) {
//            return -1;
            return -10;
        } else if (hormone.equals("সুমাছ")) {
            return -1;
        } else {
            return -1;
        }
    }

    public static double ghoniaSecondDose(String hormone, String gender, String month, double weight) {
        if (hormone.equals("পিটুইটারি গ্ল্যান্ড")) {
            if (month.equals("মার্চ-মে") && gender.equals("স্ত্রী")) {
                return 6 * weight;

            } else if (month.equals("জুন-জুলাই") && gender.equals("স্ত্রী")) {
                return 5 * weight;
            } else if (month.equals("আগস্ট-সেপ্টেম্বর") && gender.equals("স্ত্রী")) {
                return 6 * weight;
            } else if (month.equals("মার্চ-মে") && gender.equals("পুরুষ")) {
                return 2 * weight;

            } else if (month.equals("জুন-জুলাই") && gender.equals("পুরুষ")) {
                return 2 * weight;
            } else {
                return 6 * weight;
            }
        } else if (hormone.equals("ওভাপ্রিম হরমোন") || hormone.equals("ওভোপিন হরমোন") || hormone.equals("ওভাসিস টি এম হরমোন")) {
            return (float) 0.40;
        } else if (hormone.equals("সুমাছ")) {
            return -1;
        } else {
            return -1;
        }
    }


    //for carpio fish
    public static double carpioFirstDose(String hormone, String gender, String month, double weight) {
        if (hormone.equals("পিটুইটারি গ্ল্যান্ড")) {
            if (month.equals("মার্চ-মে") && gender.equals("স্ত্রী")) {
                return 1 * weight;
            } else if (month.equals("জুন-জুলাই") && gender.equals("স্ত্রী")) {
                return 1 * weight;
            } else if (month.equals("আগস্ট-সেপ্টেম্বর") && gender.equals("স্ত্রী")) {
                return 1 * weight;
            } else if (month.equals("মার্চ-মে") && gender.equals("পুরুষ")) {
                return -10;

            } else if (month.equals("জুন-জুলাই") && gender.equals("পুরুষ")) {
                return -10;
            } else {
                return 1 * weight;
            }
        } else if (hormone.equals("ওভাপ্রিম হরমোন") || hormone.equals("ওভোপিন হরমোন") || hormone.equals("ওভাসিস টি এম হরমোন")) {
//            return -1;
            return -10;
        } else if (hormone.equals("সুমাছ")) {
            return -1;
        } else {
            return -1;
        }
    }

    public static double carpioSecondDose(String hormone, String gender, String month, double weight) {
        if (hormone.equals("পিটুইটারি গ্ল্যান্ড")) {
            if (month.equals("মার্চ-মে") && gender.equals("স্ত্রী")) {
                return 5 * weight;

            } else if (month.equals("জুন-জুলাই") && gender.equals("স্ত্রী")) {
                return 5 * weight;
            } else if (month.equals("আগস্ট-সেপ্টেম্বর") && gender.equals("স্ত্রী")) {
                return 5 * weight;
            } else if (month.equals("মার্চ-মে") && gender.equals("পুরুষ")) {
                return 2 * weight;

            } else if (month.equals("জুন-জুলাই") && gender.equals("পুরুষ")) {
                return 2 * weight;
            } else {
                return 6 * weight;
            }
        } else if (hormone.equals("ওভাপ্রিম হরমোন") || hormone.equals("ওভোপিন হরমোন") || hormone.equals("ওভাসিস টি এম হরমোন")) {
            return (float) 0.40;
        } else if (hormone.equals("সুমাছ")) {
            return -1;
        } else {
            return -1;
        }
    }

    //for kalibaus fish
    public static double kalibausfirstDose(String hormone, String gender, String month, double weight) {
        if (hormone.equals("পিটুইটারি গ্ল্যান্ড")) {
            if (month.equals("মার্চ-মে") && gender.equals("স্ত্রী")) {
                return 2 * weight;
            } else if (month.equals("জুন-জুলাই") && gender.equals("স্ত্রী")) {
                return 1 * weight;
            } else if (month.equals("আগস্ট-সেপ্টেম্বর") && gender.equals("স্ত্রী")) {
                return 1 * weight;
            } else if (month.equals("মার্চ-মে") && gender.equals("পুরুষ")) {
                return -10;

            } else if (month.equals("জুন-জুলাই") && gender.equals("পুরুষ")) {
                return -10;
            } else {
                return 1 * weight;
            }
        } else if (hormone.equals("ওভাপ্রিম হরমোন") || hormone.equals("ওভোপিন হরমোন") || hormone.equals("ওভাসিস টি এম হরমোন")) {
//            return -1;
            return -10;
        } else if (hormone.equals("সুমাছ")) {
            return -1;
        } else {
            return -1;
        }
    }

    public static double kalibausSecondDose(String hormone, String gender, String month, double weight) {
        if (hormone.equals("পিটুইটারি গ্ল্যান্ড")) {
            if (month.equals("মার্চ-মে") && gender.equals("স্ত্রী")) {
                return 5 * weight;

            } else if (month.equals("জুন-জুলাই") && gender.equals("স্ত্রী")) {
                return 5 * weight;
            } else if (month.equals("আগস্ট-সেপ্টেম্বর") && gender.equals("স্ত্রী")) {
                return 6 * weight;
            } else if (month.equals("মার্চ-মে") && gender.equals("পুরুষ")) {
                return 2 * weight;

            } else if (month.equals("জুন-জুলাই") && gender.equals("পুরুষ")) {
                return 2 * weight;
            } else {
                return 6 * weight;
            }
        } else if (hormone.equals("ওভাপ্রিম হরমোন") || hormone.equals("ওভোপিন হরমোন") || hormone.equals("ওভাসিস টি এম হরমোন")) {
            return 1;
        } else if (hormone.equals("সুমাছ")) {
            return -1;
        } else {
            return -1;
        }
    }

    //for Silvar carp fish
    public static double silvarfirstDose(String hormone, String gender, String month, double weight) {
        if (hormone.equals("পিটুইটারি গ্ল্যান্ড")) {
            if (month.equals("মার্চ-মে") && gender.equals("স্ত্রী")) {
                return 2 * weight;
            } else if (month.equals("জুন-জুলাই") && gender.equals("স্ত্রী")) {
                return 2 * weight;
            } else if (month.equals("আগস্ট-সেপ্টেম্বর") && gender.equals("স্ত্রী")) {
                return 2 * weight;
            } else if (month.equals("মার্চ-মে") && gender.equals("পুরুষ")) {
                return -10;

            } else if (month.equals("জুন-জুলাই") && gender.equals("পুরুষ")) {
                return -10;
            } else {
                return 2 * weight;
            }
        } else if (hormone.equals("ওভাপ্রিম হরমোন") || hormone.equals("ওভোপিন হরমোন") || hormone.equals("ওভাসিস টি এম হরমোন")) {
////            return (float) 0.40;
            return -10;

        } else if (hormone.equals("সুমাছ")) {
            return -1;
        } else {
            return -1;
        }
    }

    public static double silvarSecondDose(String hormone, String gender, String month, double weight) {
        if (hormone.equals("পিটুইটারি গ্ল্যান্ড")) {
            if (month.equals("মার্চ-মে") && gender.equals("স্ত্রী")) {
                return 5 * weight;

            } else if (month.equals("জুন-জুলাই") && gender.equals("স্ত্রী")) {
                return 5 * weight;
            } else if (month.equals("আগস্ট-সেপ্টেম্বর") && gender.equals("স্ত্রী")) {
                return 5 * weight;
            } else if (month.equals("মার্চ-মে") && gender.equals("পুরুষ")) {
                return 2 * weight;

            } else if (month.equals("জুন-জুলাই") && gender.equals("পুরুষ")) {
                return 2 * weight;
            } else {
                return 5 * weight;
            }
        } else if (hormone.equals("ওভাপ্রিম হরমোন") || hormone.equals("ওভোপিন হরমোন") || hormone.equals("ওভাসিস টি এম হরমোন")) {
            return (float) 0.40;
        } else if (hormone.equals("সুমাছ")) {
            return -1;
        } else {
            return -1;
        }
    }


    //for bigheadcarp fish
    public static double bigheadcarpFirstDose(String hormone, String gender, String month, double weight) {
        if (hormone.equals("পিটুইটারি গ্ল্যান্ড")) {
            if (month.equals("মার্চ-মে") && gender.equals("স্ত্রী")) {
                return 2 * weight;
            } else if (month.equals("জুন-জুলাই") && gender.equals("স্ত্রী")) {
                return 2 * weight;
            } else if (month.equals("আগস্ট-সেপ্টেম্বর") && gender.equals("স্ত্রী")) {
                return 2 * weight;
            } else if (month.equals("মার্চ-মে") && gender.equals("পুরুষ")) {
                return -10;

            } else if (month.equals("জুন-জুলাই") && gender.equals("পুরুষ")) {
                return -10;
            } else {
                return 2 * weight;
            }
        } else if (hormone.equals("ওভাপ্রিম হরমোন") || hormone.equals("ওভোপিন হরমোন") || hormone.equals("ওভাসিস টি এম হরমোন")) {
//            return -1;
            return -10;
        } else if (hormone.equals("সুমাছ")) {
            return -1;
        } else {
            return -1;
        }
    }

    public static double bigheadcarpSecondDose(String hormone, String gender, String month, double weight) {
        if (hormone.equals("পিটুইটারি গ্ল্যান্ড")) {
            if (month.equals("মার্চ-মে") && gender.equals("স্ত্রী")) {
                return 5.5 * weight;

            } else if (month.equals("জুন-জুলাই") && gender.equals("স্ত্রী")) {
                return 5.5 * weight;
            } else if (month.equals("আগস্ট-সেপ্টেম্বর") && gender.equals("স্ত্রী")) {
                return 5.5 * weight;
            } else if (month.equals("মার্চ-মে") && gender.equals("পুরুষ")) {
                return 2 * weight;

            } else if (month.equals("জুন-জুলাই") && gender.equals("পুরুষ")) {
                return 2 * weight;
            } else {
                return 6 * weight;
            }
        } else if (hormone.equals("ওভাপ্রিম হরমোন") || hormone.equals("ওভোপিন হরমোন") || hormone.equals("ওভাসিস টি এম হরমোন")) {
            return (float) 0.40;
        } else if (hormone.equals("সুমাছ")) {
            return -1;
        } else {
            return -1;
        }
    }


    //for Grash carp fish
    public static double grashcarpfirstDose(String hormone, String gender, String month, double weight) {
        if (hormone.equals("পিটুইটারি গ্ল্যান্ড")) {
            if (month.equals("মার্চ-মে") && gender.equals("স্ত্রী")) {
                return (double) (1.5 * weight);
            } else if (month.equals("জুন-জুলাই") && gender.equals("স্ত্রী")) {
                return (double) (1.5 * weight);
            } else if (month.equals("আগস্ট-সেপ্টেম্বর") && gender.equals("স্ত্রী")) {
                return (double) (1.5 * weight);
            } else if (month.equals("মার্চ-মে") && gender.equals("পুরুষ")) {
                return -10;

            } else if (month.equals("জুন-জুলাই") && gender.equals("পুরুষ")) {
                return -10;
            } else {
                return (float) (1.5 * weight);
            }
        } else if (hormone.equals("ওভাপ্রিম হরমোন") || hormone.equals("ওভোপিন হরমোন") || hormone.equals("ওভাসিস টি এম হরমোন")) {
//            return (float) 0.40;
            return -10;
        } else if (hormone.equals("সুমাছ")) {
            return -1;
        } else {
            return -1;
        }
    }

    public static double grashcarpSecondDose(String hormone, String gender, String month, double weight) {
        if (hormone.equals("পিটুইটারি গ্ল্যান্ড")) {
            if (month.equals("মার্চ-মে") && gender.equals("স্ত্রী")) {
                return 4 * weight;

            } else if (month.equals("জুন-জুলাই") && gender.equals("স্ত্রী")) {
                return 4 * weight;
            } else if (month.equals("আগস্ট-সেপ্টেম্বর") && gender.equals("স্ত্রী")) {
                return 4 * weight;
            } else if (month.equals("মার্চ-মে") && gender.equals("পুরুষ")) {
                return 2 * weight;

            } else if (month.equals("জুন-জুলাই") && gender.equals("পুরুষ")) {
                return 2 * weight;
            } else {
                return 4 * weight;
            }
        } else if (hormone.equals("ওভাপ্রিম হরমোন") || hormone.equals("ওভোপিন হরমোন") || hormone.equals("ওভাসিস টি এম হরমোন")) {
            return (float) 0.40;
        } else if (hormone.equals("সুমাছ")) {
            return -1;
        } else {
            return -1;
        }
    }

    //for Rajput carp fish
    public static double rajputifirstDose(String hormone, String gender, String month, double weight) {
        if (hormone.equals("পিটুইটারি গ্ল্যান্ড")) {
            if (month.equals("মার্চ-মে") && gender.equals("স্ত্রী")) {
                return 0 * weight;
            } else if (month.equals("জুন-জুলাই") && gender.equals("স্ত্রী")) {
                return 0 * weight;
            } else if (month.equals("আগস্ট-সেপ্টেম্বর") && gender.equals("স্ত্রী")) {
                return 0 * weight;
            } else if (month.equals("মার্চ-মে") && gender.equals("পুরুষ")) {
                return -10;

            } else if (month.equals("জুন-জুলাই") && gender.equals("পুরুষ")) {
                return -10;
            } else {
                return 0 * weight;
            }
        } else if (hormone.equals("ওভাপ্রিম হরমোন") || hormone.equals("ওভোপিন হরমোন") || hormone.equals("ওভাসিস টি এম হরমোন")) {
//            return (float) 0.35;
            return -10;
        } else if (hormone.equals("সুমাছ")) {
            return -1;
        } else {
            return -1;
        }
    }

    public static double rajputiSecondDose(String hormone, String gender, String month, double weight) {
        if (hormone.equals("পিটুইটারি গ্ল্যান্ড")) {
            if (month.equals("মার্চ-মে") && gender.equals("স্ত্রী")) {
                return 5 * weight;

            } else if (month.equals("জুন-জুলাই") && gender.equals("স্ত্রী")) {
                return 5 * weight;
            } else if (month.equals("আগস্ট-সেপ্টেম্বর") && gender.equals("স্ত্রী")) {
                return 5 * weight;
            } else if (month.equals("মার্চ-মে") && gender.equals("পুরুষ")) {
                return 2 * weight;

            } else if (month.equals("জুন-জুলাই") && gender.equals("পুরুষ")) {
                return 2 * weight;
            } else {
                return 5 * weight;
            }
        } else if (hormone.equals("ওভাপ্রিম হরমোন") || hormone.equals("ওভোপিন হরমোন") || hormone.equals("ওভাসিস টি এম হরমোন")) {
            return (float) 0.35;
        } else if (hormone.equals("সুমাছ")) {
            return -1;
        } else {
            return -1;
        }
    }

    public static int grashcarpDoseInterval(String hormone) {
        if (hormone.equals("ওভাপ্রিম হরমোন") || hormone.equals("ওভোপিন হরমোন") || hormone.equals("ওভাসিস টি এম হরমোন")) {
            return 0;
        } else {
            return 6;
        }
    }

    ///

}
