package com.example.mdsuhelrana.helloandroid;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.mdsuhelrana.helloandroid.AdapterClasses.FishAdapter;
import com.example.mdsuhelrana.helloandroid.ModelClasses.Fish;
import com.example.mdsuhelrana.helloandroid.ModelClasses.ListCreation;
import com.example.mdsuhelrana.helloandroid.fish_activities.MrigelActivity;

import java.text.DecimalFormat;
import java.util.ArrayList;

public class ResultActivity extends AppCompatActivity {

    private Toolbar toolbar;
    private DrawerLayout drawer;
    private RecyclerView drawer_rv;
    private FishAdapter fishAdapter;
    private ArrayList<Fish> fishes=new ArrayList<Fish>();
    private int doseInterval;
    private int doseOvolution;
    private String fishname;

    TextView tv_fish,tv_gender,tv_weight,tv_hormone,tv_month,tv_firstdose,tv_seconddose,tv_doseinterval,tv_ovolution;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);
        init();
        createToolbar();
        fishes= ListCreation.createFishArrayList(this);

        //add drawer layout
        fishAdapter = new FishAdapter(this,fishes);
        LinearLayoutManager llm = new LinearLayoutManager(this);
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        drawer_rv.setLayoutManager(llm);
        drawer_rv.setAdapter(fishAdapter);
    }

    public void doseApply(View view) {

        final AlertDialog.Builder builder=new AlertDialog.Builder(ResultActivity.this);
        builder.setMessage("আপনি কি "+fishname+" মাছের জন্য ডোজ প্রয়োগ করতে চান ?");
        builder.setCancelable(true);
        builder.setNegativeButton("না", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.cancel();
            }
        });
        builder.setPositiveButton("হ্যাঁ", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                startActivity(new Intent(ResultActivity.this, CountdownActivity.class)
                        .putExtra("doseInterval", doseInterval)
                        .putExtra("doseOvolution", doseOvolution));
            }
        });
        AlertDialog alertDialog=builder.create();

        alertDialog.show();
    }

    private void init() {
        drawer_rv=findViewById(R.id.drawer_rv_id);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        drawer = findViewById(R.id.drawer_layout);

        //hidden button
        Button back=findViewById(R.id.tool_btn_left);
        Button next=findViewById(R.id.tool_btn_right);
        back.setVisibility(View.INVISIBLE);
        next.setVisibility(View.INVISIBLE);


        tv_fish=findViewById(R.id.rfishname);
        tv_gender=findViewById(R.id.rgender);
        tv_weight=findViewById(R.id.rweight);
        tv_hormone=findViewById(R.id.rhormone);
        tv_firstdose=findViewById(R.id.rfirstdose);
        tv_seconddose=findViewById(R.id.rseconddose);
        tv_doseinterval=findViewById(R.id.rdoseinterval);
        tv_month=findViewById(R.id.rmonth);
        tv_ovolution=findViewById(R.id.rovolution);

        fishname=getIntent().getStringExtra("fish");
        String gender= getIntent().getStringExtra("gender");
        String month= getIntent().getStringExtra("month");
        String hormone= getIntent().getStringExtra("hormone");

        double weight= Double.valueOf(getIntent().getStringExtra("weight"));
        double firstDose= getIntent().getDoubleExtra("firstDose",0);
        firstDose = Double.parseDouble(new DecimalFormat("#.###").format(firstDose));
        double secondDose= getIntent().getDoubleExtra("secondDose",0);
        secondDose = Double.valueOf(new DecimalFormat("#.###").format(secondDose));

        doseInterval= getIntent().getIntExtra("doseInterval",0);
        doseOvolution= getIntent().getIntExtra("ovolution",0);

        tv_fish.setText(fishname);
        tv_gender.setText(gender);
        tv_month.setText(month);
        tv_hormone.setText(hormone);
        tv_weight.setText(new DecimalFormat("#.###").format(weight) +" কে.জি");
        if(firstDose!=0){
            tv_firstdose.setText(firstDose+" মি.গ্রা");
        }else {
            tv_firstdose.setText("--- --- ---");
        }
        if(secondDose!=0) {
            tv_seconddose.setText(secondDose + " মি.গ্রা");
        }else {
            tv_seconddose.setText("--- --- ---");
        }
        if(doseInterval!=0){
            tv_doseinterval.setText(doseInterval+" ঘণ্টা");
        }else {
            tv_doseinterval.setText("--- --- ---");
        }
        tv_ovolution.setText(doseOvolution+" ঘণ্টা");
    }
    private void createToolbar() {
        toolbar.setBackgroundColor(Color.parseColor("#09dba2"));
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(null);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        View decor = getWindow().getDecorView();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            getWindow().setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark2, this.getTheme()));

        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark2));
        }

    }

    public void activity_change(View view) {
        switch (view.getId()){
            case R.id.toolbtn_home:
                startActivity(new Intent(ResultActivity.this, MainActivity.class));
                break;
           /* case R.id.tool_btn_left:
                startActivity(new Intent(ResultActivity.this, MrigelActivity.class));
                break;
            case R.id.tool_btn_right:
                startActivity(new Intent(ResultActivity.this,CountdownActivity.class));
                break;*/
        }
    }
    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    public void fabClick(View view) {
        startActivity(new Intent(this,FormulaActivity.class));
    }
}
